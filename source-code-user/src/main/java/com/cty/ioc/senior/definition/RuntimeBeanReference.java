package com.cty.ioc.senior.definition;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 引用类型值描述类
 *
 * @author : cty
 * @since 2023/11/12
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RuntimeBeanReference {

    /**
     * 另一个bean的唯一标识
     */
    private String ref;
}
