package com.cty.ioc.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 英雄技能
 *
 * @author : cty
 * @since 2023/10/16
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Skill {

    public void initMethod() {
        System.out.println("开始创建Skill对象-------->");
    }

    /**
     * 技能名称
     */
    private String name;

    /**
     * 技能伤害
     */
    private Integer damage;

}
