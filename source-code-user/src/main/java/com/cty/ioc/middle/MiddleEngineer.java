package com.cty.ioc.middle;

import com.cty.ioc.bean.Hero;
import com.cty.ioc.bean.Skill;

/**
 * 中级工程师
 *
 * @author : cty
 * @since 2023/10/16
 */


public class MiddleEngineer {

    /**
     * 根据对象名称获取对象
     * @param objectName 对象名称
     * @return 对象
     */
    public Object getObject(String objectName) {
        if ("hero".equals(objectName)) {
            Skill skill = new Skill("王者圣剑", 1000);
            Hero hero = new Hero("亚瑟", 1888, skill);
            return hero;
        } else if ("other".equals(objectName)) {
            return null;
        } else {
            return null;
        }
    }
}
