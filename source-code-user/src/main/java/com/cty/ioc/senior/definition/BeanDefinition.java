package com.cty.ioc.senior.definition;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

/**
 * 对象定义
 *
 * @author : cty
 * @since 2023/11/11
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BeanDefinition {

    /**
     * 唯一标识
     */
    private String id;

    /**
     * 类全路径名称
     */
    private String className;

    /**
     * 类对象
     */
    private Class classType;

    /**
     * 类初始化方法名称
     */
    private String initMethod;

    /**
     * 类属性定义列表
     */
    private List<PropertyValue> propertyValueList;

    /**
     * 管理方式
     * 单例 singleton
     * 原型 prototype
     */
    private String scope;

    public boolean isSingleton() {
        return "singleton".equals(this.scope);
    }

    /**
     * 添加属性定义对象
     *
     * @param propertyValue 属性定义对象
     */
    public void addPropertyValue(PropertyValue propertyValue) {
        if (propertyValueList == null) {
            this.propertyValueList = new ArrayList<>();
        }
        this.propertyValueList.add(propertyValue);
    }
}
