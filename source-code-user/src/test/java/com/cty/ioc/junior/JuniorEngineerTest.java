package com.cty.ioc.junior;

import com.cty.ioc.bean.Hero;
import org.junit.Test;


public class JuniorEngineerTest {

    @Test
    public void getHeroBean() {
        JuniorEngineer juniorEngineer = new JuniorEngineer();
        Hero hero = juniorEngineer.getHeroObject();
        hero.releaseSkill();
    }
}