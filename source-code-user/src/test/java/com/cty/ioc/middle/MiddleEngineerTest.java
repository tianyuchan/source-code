package com.cty.ioc.middle;

import com.cty.ioc.bean.Hero;
import org.junit.Test;


public class MiddleEngineerTest {

    @Test
    public void getObject() {
        MiddleEngineer middleEngineer = new MiddleEngineer();
        Hero hero = (Hero) middleEngineer.getObject("hero");
        hero.releaseSkill();
    }
}