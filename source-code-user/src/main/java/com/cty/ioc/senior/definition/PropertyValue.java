package com.cty.ioc.senior.definition;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 属性定义
 *
 * @author : cty
 * @since 2023/11/12
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PropertyValue {

    /**
     * 属性名称
     */
    private String name;

    /**
     * 属性值
     * 简单类型 TypedStringValue
     * 引用类型 RuntimeBeanReference
     */
    private Object value;

}
