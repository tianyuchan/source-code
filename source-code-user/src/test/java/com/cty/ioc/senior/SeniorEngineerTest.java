package com.cty.ioc.senior;

import com.cty.ioc.bean.Hero;
import org.junit.Test;


public class SeniorEngineerTest {

    @Test
    public void getBean() throws ClassNotFoundException {
        SeniorEngineer seniorEngineer = new SeniorEngineer();
        Hero hero = (Hero) seniorEngineer.getBean("hero");
        hero.releaseSkill();
    }
}