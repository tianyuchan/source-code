package com.cty.ioc.senior.definition;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 简单类型值描述类
 *
 * @author : cty
 * @since 2023/11/12
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TypedStringValue {

    /**
     * 值的字符串
     */
    private String value;

    /**
     * 值的类型
     */
    private Class classType;
}
