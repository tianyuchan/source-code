package com.cty.ioc.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 游戏英雄角色
 *
 * @author : cty
 * @since 2023/10/16
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Hero {

    public void initMethod() {
        System.out.println("开始创建Hero对象-------->");
    }

    /**
     * 英雄名称
     */
    private String name;

    /**
     * 英雄金币价格
     */
    private Integer price;

    /**
     * 英雄技能
     */
    private Skill skill;


    /**
     * 释放技能
     */
    public void releaseSkill() {
        System.out.println(this.name + "释放技能" + this.skill.getName() + "，打出了" + this.skill.getDamage() + "点伤害");
    }

}
