package com.cty.ioc.junior;

import com.cty.ioc.bean.Hero;
import com.cty.ioc.bean.Skill;

/**
 * 初级工程师
 *
 * @author : cty
 * @since 2023/10/16
 */


public class JuniorEngineer {

    /**
     * 获取Hero对象
     * @return Hero对象
     */
    public Hero getHeroObject() {
        Skill skill = new Skill("王者圣剑", 1000);
        Hero hero = new Hero("亚瑟", 1888, skill);
        return hero;
    }

}
